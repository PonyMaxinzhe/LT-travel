package com.lt.base.upmsapi.dto;

import lombok.Data;

/**
 * SystemUserBaseDto对象
 *
 * @Author ToneyMa
 * @Date 2023-03-03 23:22:45
 */
@Data
public class SysUserBaseInfoDto {
}
