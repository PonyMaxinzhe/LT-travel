package com.lt.base.upmsapi.client;

import org.springframework.cloud.openfeign.FeignClient;

/**
 * 用户基础数据操作访问接口。
 *
 * @Author ToneyMa
 * @Date 2023-03-03 21:42:51
 */
@FeignClient(
        name = "system"
)
public interface SysUserBaseInfoClient {
}
