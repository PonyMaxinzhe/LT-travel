package com.lt.base.controller;

import com.lt.base.model.SysUserBaseInfo;
import com.lt.common.core.base.object.ResponseResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 系统账号Controller
 *
 * @Author ToneyMa
 * @Date 2023-02-23 22:15:10
 */
@RestController
public class SystemUserController {
    @PostMapping("/hello")
    public String getHellow() {
        String s = "aaaa";
        return s;
    }

    @PostMapping("/insertAdmin")
    public ResponseResult<SysUserBaseInfo> insertAdmin() {
        return ResponseResult.success(new SysUserBaseInfo());
    }
}
