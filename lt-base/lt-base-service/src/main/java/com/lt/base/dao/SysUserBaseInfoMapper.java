package com.lt.base.dao;

import com.lt.base.model.SysUserBaseInfo;
import com.lt.common.core.base.dao.BaseDaoMapper;
import org.apache.ibatis.annotations.Param;

/**
 * 系统用户信息数据操作访问接口。
 *
 * @Author ToneyMa
 * @Date 2023-03-05 18:02:29
 */
public interface SysUserBaseInfoMapper extends BaseDaoMapper<SysUserBaseInfo> {
    int saveNew(@Param("entity") SysUserBaseInfo entity);
    long selectCount();
}
