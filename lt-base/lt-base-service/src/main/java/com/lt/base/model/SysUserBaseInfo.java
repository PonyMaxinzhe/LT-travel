package com.lt.base.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.lt.common.core.base.model.BaseModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("sys_user_base_info")
public class SysUserBaseInfo extends BaseModel {

    @TableId(type = IdType.AUTO)
    private Long userId;

    /**
     * 昵称
     */
    private String nickName;

    /**
     * 用户名字
     */
    private String userName;

    /**
     * 用户手机号
     */
    private String userTel;

    /**
     * 登录账号
     */
    private String account;

    /**
     * 邮箱
     */
    private String email;

    /**
     * 密码
     */
    private String password;

    /**
     * 组织id
     */
    private Long orgId;

}
