package com.lt.base;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;

import javax.annotation.PostConstruct;
import java.util.HashMap;

/**
 * 系统管理启动类
 *
 * @Author ToneyMa
 * @Date 2023-02-23 22:23:20
 */

@EnableAutoConfiguration(exclude = {DataSourceAutoConfiguration.class})
@EnableFeignClients(basePackages = "com.lt")
@EnableDiscoveryClient
@ComponentScan("com.lt")
@MapperScan("com.lt.base.dao")
public class SystemApplication {
    public static void main(String[] args) {
        SpringApplication.run(SystemApplication.class, args);
        HashMap<String, String> hashMap = new HashMap<>();
    }

    @PostConstruct
    public void initCache(){

    }
}

