package com.lt.base.service;

import com.lt.base.model.SysUserBaseInfo;
import com.lt.base.upmsapi.dto.SysUserBaseInfoDto;
import com.lt.common.core.base.service.IBaseService;

/**
 * 用户基本信息服务接口
 *
 * @Author ToneyMa
 * @Date 2023-02-26 20:53:43
 */
public interface SysUserBaseInfoService extends IBaseService<SysUserBaseInfo, Long> {
    void saveNew(SysUserBaseInfoDto systemUserBaseDto);
}
