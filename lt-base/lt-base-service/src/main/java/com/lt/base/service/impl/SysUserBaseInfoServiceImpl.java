package com.lt.base.service.impl;

import com.lt.base.model.SysUserBaseInfo;
import com.lt.base.service.SysUserBaseInfoService;
import com.lt.base.upmsapi.dto.SysUserBaseInfoDto;
import com.lt.common.core.base.service.BaseService;
import org.springframework.stereotype.Service;

/**
 * 用户基本信息服务
 *
 * @Author ToneyMa
 * @Date 2023-02-26 20:54:09
 */
@Service
public class SysUserBaseInfoServiceImpl extends BaseService<SysUserBaseInfo, Long> implements SysUserBaseInfoService {

    @Override
    public void saveNew(SysUserBaseInfoDto systemUserBaseDto) {

    }
}
