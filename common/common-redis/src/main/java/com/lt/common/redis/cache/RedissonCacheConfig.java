package com.lt.common.redis.cache;

import com.google.common.collect.Maps;
import org.redisson.api.RedissonClient;
import org.redisson.spring.cache.CacheConfig;
import org.redisson.spring.cache.RedissonSpringCacheManager;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Map;

/**
 * 使用Redisson作为Redis的分布式缓存库。
 *
 * @Author ToneyMa
 * @Date 2023-04-10 22:00:35
 */
@Configuration
@EnableCaching
public class RedissonCacheConfig {
    /**
     * 默认的缓存过期时间，单位：毫秒。
     */
    private static final int DEFAULT_TTL = 3600000;

    /**
     * 定义SpringCache的各种信息。
     */
    public enum CacheEnum{
        /**
         * session下上传文件名的缓存(默认24小时)。
         */
        UPLOAD_FILENAME_CACHE(86400000),

        /**
         * 全局缓存时间(默认24小时)。
         */
        GLOBAL_CACHE(86400000);

        /**
         * 缓存的时长(单位：毫秒)
         */
        private int ttl = DEFAULT_TTL;

        // 构造函数。
        CacheEnum(int ttl) {
            this.ttl = ttl;
        }

        // 提供getTTL的方法。
        public int getTtl() {
            return ttl;
        }
    }

    @Bean
    CacheManager cacheManager(RedissonClient redissonClient) {
        Map<String, CacheConfig> config = Maps.newHashMap();
        // 初始化缓存配置。
        for (CacheEnum e : CacheEnum.values()) {
            // 设置过期时间为枚举中设置的ttl，最长空闲时间为0。
            config.put(e.name(), new CacheConfig(e.getTtl(), 0));
        }
        // 返回RedissonSpringCacheManager，让SpringCache注解使用Redisson来处理缓存。
        // 具体源码请参考RedissonSpringCacheManager如何实现CacheManager接口。
        return new RedissonSpringCacheManager(redissonClient, config);
    }
}
