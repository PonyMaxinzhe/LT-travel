package com.lt.common.redis.config;

import cn.hutool.core.util.ArrayUtil;
import cn.hutool.core.util.StrUtil;
import com.lt.common.core.exception.InvalidRedisModeException;
import org.redisson.Redisson;
import org.redisson.api.RedissonClient;
import org.redisson.config.Config;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Redisson配置类。
 *
 * @Author ToneyMa
 * @Date 2023-04-10 00:58:25
 */
@Configuration
@ConditionalOnProperty(name = "redis.redisson.enabled", havingValue = "true")
public class RedissonConfig {

    /**
     * 分布式锁超时时间，单位：毫秒。
     */
    @Value("${redis.redisson.lockWatchdogTimeout}")
    private Integer lockWatchdogTimeout;

    /**
     * Redis模式，支持single、cluster、sentinel、master-slave。
     */
    @Value("${redis.redisson.mode}")
    private String mode;

    /**
     * 仅仅用于sentinel模式。
     */
    @Value("${redis.redisson.masterName:}")
    private String masterName;

    /**
     * Redis地址，多个地址间，用逗号分隔。
     */
    @Value("${redis.redisson.address}")
    private String address;

    /**
     * 默认超时时间，单位：毫秒。
     */
    @Value("${redis.redisson.timeout}")
    private Integer timeout;

    /**
     * Redis密码。
     */
    @Value("${redis.redisson.password}")
    private String password;

    /**
     * 连接池大小。
     */
    @Value("${redis.redisson.pool.poolSize}")
    private Integer poolSize;

    /**
     * 最小空闲连接数。
     */
    @Value("${redis.redisson.pool.minIdle}")
    private Integer minIdle;

    @Bean
    public RedissonClient redissonClient() {
        if (StrUtil.isBlank(password)) {
            password = null;
        }
        Config config = new Config();
        // 注意：这里的switch语句，是JDK 7的新特性，不支持JDK 6。如果使用的是jdk7以下的版本，请改为if-else判断，否则会抛出异常。
        // 并且会判断大小写，请在配置文件中，使用全小写配置。
        switch (mode) {
            case "single":
                config.setLockWatchdogTimeout(lockWatchdogTimeout)
                        .useSingleServer()
                        .setAddress(address)
                        .setPassword(password)
                        .setConnectTimeout(timeout)
                        .setConnectionPoolSize(poolSize)
                        .setConnectionMinimumIdleSize(minIdle);
                break;
            case "cluster":
                String[] clusterAddresses = StrUtil.splitToArray(address, ",");
                config.setLockWatchdogTimeout(lockWatchdogTimeout)
                        .useClusterServers()
                        .setPassword(password)
                        .setConnectTimeout(timeout)
                        .addNodeAddress(clusterAddresses)
                        .setMasterConnectionPoolSize(poolSize)
                        .setMasterConnectionMinimumIdleSize(minIdle);
                break;
            case "sentinel":
                String[] sentinelAddresses = StrUtil.splitToArray(address, ",");
                config.setLockWatchdogTimeout(lockWatchdogTimeout)
                        .useSentinelServers()
                        .setMasterName(masterName)
                        .setPassword(password)
                        .setConnectTimeout(timeout)
                        .addSentinelAddress(sentinelAddresses)
                        .setMasterConnectionPoolSize(poolSize)
                        .setMasterConnectionMinimumIdleSize(minIdle);
                break;
            case "master-slave":
                String[] masterSlaveAddresses = StrUtil.splitToArray(address, ",");
                if (masterSlaveAddresses.length == 1) {
                    throw new IllegalArgumentException(
                            "redis.redisson.address MUST have multiple redis addresses for master-slave mode.");
                }
                String[] slaveAddresses = new String[masterSlaveAddresses.length - 1];
                ArrayUtil.copy(masterSlaveAddresses, 1, slaveAddresses, 0, slaveAddresses.length);
                config.setLockWatchdogTimeout(lockWatchdogTimeout)
                        .useMasterSlaveServers()
                        .setMasterAddress(masterSlaveAddresses[0])
                        .addSlaveAddress(slaveAddresses)
                        .setPassword(password)
                        .setConnectTimeout(timeout)
                        .setMasterConnectionPoolSize(poolSize)
                        .setMasterConnectionMinimumIdleSize(minIdle);
                break;
            default:
                throw new InvalidRedisModeException(mode);
        }
        return Redisson.create(config);
    }
}

