package com.lt.common.log.config;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

/**
 * common-log日志模块自动配置引导类。
 *
 * @Author ToneyMa
 * @Date 2023-07-12
 **/
@EnableConfigurationProperties({OperationLogProperties.class})
public class CommonLogAutoConfig {
}
