package com.lt.common.log.model.constant;

import java.util.HashMap;
import java.util.Map;

/**
 * 操作日志类型常量字典对象
 *
 * @Author ToneyMa
 * @Date 2023-02-23 20:08:31
 */
public final class SysOperationLogType {

    /**
     * 其他
     */
    public final static int OTHER = -1;

    /**
     * 登录
     */
    public final static int LOGIN = 0;

    /**
     * 登出
     */
    public final static int LOGOUT = 5;

    /**
     * 查询
     */
    public final static int LIST = 10;

    /**
     * 分组查询
     */
    public final static int LIST_WITH_GROUP = 11;

    /**
     * 新增
     */
    public final static int ADD = 15;

    /**
     * 删除
     */
    public final static int DELETE = 20;

    /**
     * 批量删除
     */
    public final static int DELETE_BATCH = 21;

    /**
     * 修改
     */
    public final static int UPDATE = 25;

    private static final Map<Object, String> DICT_MAP = new HashMap<>();

    static {
        DICT_MAP.put(OTHER,"其他");
        DICT_MAP.put(LOGIN,"登录");
        DICT_MAP.put(LOGOUT,"登出");
        DICT_MAP.put(LIST,"查询");
        DICT_MAP.put(LIST_WITH_GROUP,"分组查询");
        DICT_MAP.put(ADD,"新增");
        DICT_MAP.put(DELETE,"删除");
        DICT_MAP.put(DELETE_BATCH,"批量删除");
        DICT_MAP.put(UPDATE,"更新");
    }
}
