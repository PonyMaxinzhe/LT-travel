package com.lt.common.log.aop;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSONObject;
import com.lt.common.core.constant.ApplicationConstant;
import com.lt.common.core.util.ContextUtil;
import com.lt.common.core.util.MyCommonUtil;
import com.lt.common.log.annotation.OperationLog;
import com.lt.common.log.config.OperationLogProperties;
import com.lt.common.log.model.SysOperationLog;
import com.lt.comon.sequence.generator.IdGenerator;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.annotation.Order;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.lang.reflect.Method;
import java.util.List;

/**
 * 操作日志增强切面(该切面应最后执行)
 *
 * @Author ToneyMa
 * @Date 2023-02-23 20:23:59
 */
@Aspect
@Component
@Slf4j
@Order(Integer.MAX_VALUE - 1)
public class OperationLogAspect {

    @Value("#{'${interface.warntime}'?:5000}")
    private long warntime;

    @Resource
    private OperationLogProperties properties;

    @Resource
    private KafkaTemplate<String, Object> kafkaTemplate;

    // TODO: 2023/7/13 注入ID生成器

    @Pointcut("execution(public * com.lt..controller..*(..))")
    public void operationLogPointCut() {
        // 配置到所有的controller中
    }

    @Around("operationLogPointCut()")
    public Object around(ProceedingJoinPoint joinPoint) throws Throwable {
        // 计时
        long interfaceStartTime = System.currentTimeMillis();
        HttpServletRequest request = ContextUtil.getHttpRequest();
        HttpServletResponse response = ContextUtil.getHttpResponse();
        String traceId = this.getTraceId(request);
        // 设置traceId
        request.setAttribute(ApplicationConstant.HTTP_HEADER_TRACE_ID, traceId);
        // 将流水号返回给前端以便定位问题
        response.setHeader(ApplicationConstant.HTTP_HEADER_TRACE_ID, traceId);
        MDC.put(ApplicationConstant.HTTP_HEADER_TRACE_ID, traceId);
        // TODO: 2023/7/12 Token机制还未完成，需要往上下文调试对象MDC中记录Token中解析出来的SessionId和UserId
//        TokenData tokenData = TokenData.takeFromRequest();
//        // 为日志框架设定变量，使日志可以输出更多有价值的信息。
//        if (tokenData != null) {
//            MDC.put("sessionId", tokenData.getSessionId());
//            MDC.put("userId", tokenData.getUserId().toString());
//        }
        // 获取方法参数名
        String[] parameterNames = this.getParameterNames(joinPoint);
        // 获取各个参数类型
        Object[] args = joinPoint.getArgs();
        JSONObject jsonArgs = new JSONObject();
        // 装配参数名和参数类型
        for (int i = 0; i < args.length; i++) {
            Object arg = args[i];
            // 判断是否为普通类型参数
            if (this.isNormalArgs(arg)) {
                // 是，则加入JSONObject参数中。
                jsonArgs.put(parameterNames[i], arg);
            }
        }
        // 转换为JSON字符串用于保存
        String params = jsonArgs.toJSONString();

        SysOperationLog entity = null;
        OperationLog operationLogAnnotation = null;

        // 检查日志是否开启
        boolean saveOperationLog = properties.isEnabled();
        if (saveOperationLog) {
            operationLogAnnotation = this.getOperationLogAnnotation(joinPoint);
            // 检查是否存在注解
            saveOperationLog = (operationLogAnnotation != null);
        }
        if (saveOperationLog) {
            // TODO: 2023/7/12 构造操作日志Entity实体
//            this.buildSysOperationLog(operationLogAnnotation, joinPoint, params, traceId, tokenData);
        }

        // 执行方法
        Object result = joinPoint.proceed();

        long interfaceExecuteTime = System.currentTimeMillis() - interfaceStartTime;
        if (interfaceExecuteTime >= warntime) {
            log.warn("接口执行时间过长，接口地址：{}，执行时间：{}ms", request.getRequestURI(), interfaceExecuteTime);
        }
        return result;
    }

    private String getTraceId(HttpServletRequest request) {
        String traceId = request.getHeader(ApplicationConstant.HTTP_HEADER_TRACE_ID);
        if (StrUtil.isEmpty(traceId)) {
            traceId = MyCommonUtil.generateUuid();
        }
        return traceId;
    }

    /**
     * 获取方法中的各个参数名。
     *
     * @param joinPoint 切点
     * @return 方法中传参的参数名字符串数组。
     */
    private String[] getParameterNames(ProceedingJoinPoint joinPoint) {
        MethodSignature methodSignature = (MethodSignature) joinPoint.getSignature();
        return methodSignature.getParameterNames();
    }

    /**
     * 获取方法上的日志注解。
     *
     * @param joinPoint 切点
     * @return 日志注解
     */
    private OperationLog getOperationLogAnnotation(ProceedingJoinPoint joinPoint) {
        MethodSignature methodSignature = (MethodSignature) joinPoint.getSignature();
        Method method = methodSignature.getMethod();
        return method.getAnnotation(OperationLog.class);
    }

    /**
     * 判断是否为普通参数。
     *
     * @param o 参数
     * @return 是否为普通参数
     */
    private boolean isNormalArgs(Object o) {
        if (o instanceof List) {
            List<?> list = (List<?>) o;
            if (CollUtil.isNotEmpty(list)) {
                // 如果是文件则不是NormalArgs，返回false。
                return !(list.get(0) instanceof MultipartFile);
            }
        }
        // 判断是否为Request或Response类型或文件类型
        return !(o instanceof HttpServletResponse)
                && !(o instanceof HttpServletRequest)
                && !(o instanceof MultipartFile);
    }
}
