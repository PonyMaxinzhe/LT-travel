package com.lt.common.log.model;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;

import java.time.LocalDateTime;

/**
 * SysOperationLog操作日志Model
 *
 * @Author ToneyMa
 * @Date 2023-07-12
 **/

@Data
@Accessors(chain = true)
@TableName("sys_operation_log")
public class SysOperationLog {

    @TableId(value = "log_id")
    private Long logId;

    /**
     * 日志描述
     */
    private String description;

    /**
     * 操作类型，SysOperationLogConstant类
     */
    private Integer operationType;

    /**
     * 接口所在的服务名，通常为spring.application.name配置项的值。
     */
    private String serviceName;

    /**
     * 调用的Controller全类名，之所以要独立保存，是为了便于统计接口的调用额度
     */
    private String apiClass;

    /**
     * 调用的Controller中的方法，格式为：接口类名+“.”+方法名。
     */
    private String apiMethod;

    /**
     * 用户会话SessionId，主要是为了便于统计以及跟踪查询定位问题。
     */
    private String sessionId;

    /**
     * 调用时长
     */
    private Long elapse;

    /**
     * HTTP请求方法类型：如GET
     */
    private String requestMethod;

    /**
     * HTTP请求地址
     */
    private String requestUrl;

    /**
     * 接口参数
     */
    private String requestArguments;

    /**
     * 应答结果
     */
    private String responseResult;

    /**
     * 请求IP
     */
    private String requestIp;

    /**
     * 应答状态
     */
    private Boolean success;

    /**
     * 错误信息
     */
    private String errorMsg;

    /**
     * 操作员id
     */
    private Long operatorId;

    /**
     * 操作人名称
     */
    private String operatorName;

    /**
     * 操作时间
     */
    private LocalDateTime operationTime;

    /**
     * 调用时长最小值。
     */
    @TableField(exist = false)
    private Long elapseMin;

    /**
     * 调用时长最大值。
     */
    @TableField(exist = false)
    private Long elapseMax;

    /**
     * 操作开始时间。
     */
    @TableField(exist = false)
    private String operationTimeStart;

    /**
     * 操作结束时间。
     */
    @TableField(exist = false)
    private String operationTimeEnd;
}