package com.lt.common.log.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * 操作日志的配置类。
 *
 * @Author ToneyMa
 * @Date 2023-07-12
 **/
@Data
@ConfigurationProperties(prefix = "common-log.operation-log")
public class OperationLogProperties {
    /**
     * 是否采集操作日志。
     */
    private boolean enabled = true;

    /**
     * kafka topic
     */
    private String kafkaTopic = "SysOperationLog";
}
