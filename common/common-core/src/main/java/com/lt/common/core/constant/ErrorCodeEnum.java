package com.lt.common.core.constant;

/**
 * @Author ToneyMa
 * @Date 2023-02-26 20:46:36
 */
public enum ErrorCodeEnum {
    /**
     * 没有错误
     */
    NO_ERROR("没有错误"),
    /**
     * 未处理的异常
     */
    UNHANDLED_EXCEPTION("未处理的异常"),
    DATA_VALIDATED_FAILED("数据验证失败，请核对！"),
    RPC_DATA_ACCESS_FAILED("远程调用数据访问失败，请联系管理员！"),
    NO_OPERATION_PERMISSION("当前用户没有操作权限，请核对！"),
    UNAUTHORIZED_LOGIN("当前用户尚未登录或登录已超时，请重新登录！");

    /**
     * 构造函数
     *
     * @param errorMessage 构造错误信息
     */
    ErrorCodeEnum(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    /**
     * 错误信息
     */
    private final String errorMessage;

    /**
     * 获取错误信息。
     *
     * @return 错误信息。
     */
    public String getErrorMessage() {
        return errorMessage;
    }
}
