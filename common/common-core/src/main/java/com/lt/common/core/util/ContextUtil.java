package com.lt.common.core.util;

import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 获取Servlet HttpRequest和HttpResponse的工具类。
 *
 * @Author ToneyMa
 * @Date 2023-02-23 21:14:15
 */
public class ContextUtil {
    /**
     * 判断是否为Http上下文环境
     *
     * @return 结果
     */
    public static boolean hasRequestContext() {
        return RequestContextHolder.getRequestAttributes() != null;
    }

    /**
     * 获取Servlet请求上下文的HttpRequest对象
     *
     * @return 请求上下文HttpRequest对象
     */
    public static HttpServletRequest getHttpRequest() {
        return ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
    }
    /**
     * 获取Servlet请求上下文的HttpResponse对象
     *
     * @return 请求上下文HttpResponse对象
     */
    public static HttpServletResponse getHttpResponse() {
        return ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getResponse();
    }

    private ContextUtil() {
        // 私有化构造函数，避免被外部实例化
    }
}
