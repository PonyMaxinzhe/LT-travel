package com.lt.common.core.constant.amap;

/**
 * 高德地图API调用状态常量
 *
 * @Author: ToneyMa
 * @Date: 2023-06-05 09:21
 **/

public final class MapStatusContant {
    public final static Integer SUCCESS = 1;
    public final static Integer FAIL = 0;
}
