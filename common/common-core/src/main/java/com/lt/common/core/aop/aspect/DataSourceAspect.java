package com.lt.common.core.aop.aspect;

import com.lt.common.core.aop.annotation.MyDataSource;
import com.lt.common.core.config.DataSourceContextHolder;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

/**
 * 多数据源AOP切面类。
 *
 * @Author: ToneyMa
 * @Date: 2023-07-06 15:11
 **/
@Aspect
@Component
@Order(Integer.MIN_VALUE + 1)
@Slf4j
public class DataSourceAspect {

    /**
     * 所有配置DataSource注解的Service方法都会被拦截。
     */
    @Pointcut("execution(public * com.lt..service..*(..))" +
            "&& @target(com.lt.common.core.aop.annotation.MyDataSource)")
    public void datasourcePointCut(){}

    @Around("datasourcePointCut()")
    public Object around(ProceedingJoinPoint point) throws Throwable {
        Class<?> clazz = point.getTarget().getClass();
        MyDataSource ds = clazz.getAnnotation(MyDataSource.class);
        // 通过判断 DataSource 中的值来判断当前方法应用哪个数据源
        Integer originalType = DataSourceContextHolder.setDataSourceType(ds.value());
        log.debug("set datasource is " + ds.value());
        try {
            return point.proceed();
        } finally {
            // 清除数据源信息
            DataSourceContextHolder.unset(originalType);
            log.debug("unset datasource is " + originalType);
        }
    }
}