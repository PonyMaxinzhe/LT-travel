package com.lt.common.core.constant;

/**
 * 代理类型常量
 *
 * @Author ToneyMa
 * @Date 2023-04-02 13:10:02
 */
public final class ProxyTypeConstant {
    public final static String SQUID_PROXY = "X-Forwarded-For";
    public final static String APACHE_PROXY = "Proxy-Client-IP";
    public final static String HTTPXFF_PROXY = "HTTP_X_FORWARDED_FOR";
    public final static String WEBLOGIC_PROXY = "WL-Proxy-Client-IP";
    public final static String OTHER_PROXY = "HTTP_CLIENT_IP";
    public final static String NGINX_PROXY = "X-Real-IP";
}
