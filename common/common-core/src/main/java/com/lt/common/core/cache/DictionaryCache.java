package com.lt.common.core.cache;

import java.util.List;
import java.util.Set;

/**
 * 字典缓存接口
 *
 * @param <K> 字典表主键类型。
 * @param <V> 字典表对象类型。
 * @Author: ToneyMa
 * @Date: 2023-05-30 16:11
 **/

public interface DictionaryCache<K, V> {
    /**
     * 按照数据插入的顺序返回全部字典对象的列表。
     *
     * @return 字典表对象列表。
     */
    List<V> getAll();

    /**
     * 获取缓存中与键列表对应的对象列表。
     *
     * @param keys 字典缓存主键集合。
     * @return 字典表对象列表。
     */
    List<V> getInList(Set<K> keys);

    /**
     * 从缓存中获取指定的数据。
     *
     * @param key 数据的key。
     * @return 获取到的数据，如果没有返回null。
     */
    V get(K key);

    /**
     * 将数据存入缓存。
     *
     * @param key    通常为字典数据的主键。
     * @param object 字典数据对象。
     */
    void put(K key, V object);
}
