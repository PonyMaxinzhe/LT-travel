package com.lt.common.core.constant;

/**
 * 全局逻辑删除标识常量
 *
 * @Author ToneyMa
 * @Date 2023-02-26 20:18:53
 */
public final class GlobalDeletedFlag {

    /**
     * 表示数据表记录已经删除
     */
    public static final int DELETED = -1;
    /**
     * 数据记录正常
     */
    public static final int NORMAL = 1;

    /**
     * 私有构造函数，明确标识该常量类的作用。
     */
    private GlobalDeletedFlag() {
    }
}
