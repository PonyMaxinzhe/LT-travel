package com.lt.common.core.base.service;

import com.baomidou.mybatisplus.extension.service.IService;

import java.io.Serializable;
import java.util.function.BiConsumer;
import java.util.function.Consumer;

/**
 * 所有Service的基础Service接口
 *
 * @param <M> Model对象类型
 * @param <K> Model的主键类型
 */
public interface IBaseService<M, K extends Serializable> extends IService<M> {

    /**
     * 如果主键存在则更新，否则新增保存实体对象。
     *
     * @param data    实体对象数据。
     * @param saveNew 新增实体对象方法。
     * @param update  更新实体对象方法。
     */
    void saveNewOrUpdate(M data, Consumer<M> saveNew, BiConsumer<M, M> update);
}
