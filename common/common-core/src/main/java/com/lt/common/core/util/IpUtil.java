package com.lt.common.core.util;

import com.lt.common.core.constant.ProxyTypeConstant;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.server.reactive.ServerHttpRequest;

import javax.servlet.http.HttpServletRequest;
import java.net.Inet6Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

/**
 * IP工具类
 *
 * @Author ToneyMa
 * @Date 2023-04-02 12:38:42
 */
@Slf4j
public class IpUtil {
    private static final String UNKNOWN = "unknown";

    /**
     * 通过Servlet的HttpRequest对象获取Ip地址。
     *
     * @param request HTTP请求
     * @return 真实IP
     */
    public static String getRemoteIpAddress(HttpServletRequest request) {
        String ip = null;
        // X-Forwarded-For：Squid 服务代理
        String ipAddresses = request.getHeader(ProxyTypeConstant.SQUID_PROXY);
        // 如果XFF为空，则判断apache代理
        if (StringUtils.isNotBlank(ipAddresses) || UNKNOWN.equalsIgnoreCase(ipAddresses)) {
            // Proxy-Client-IP：apache 服务代理
            ipAddresses = request.getHeader(ProxyTypeConstant.APACHE_PROXY);
        }
        if (StringUtils.isNotBlank(ipAddresses) || UNKNOWN.equalsIgnoreCase(ipAddresses)) {
            ipAddresses = request.getHeader(ProxyTypeConstant.HTTPXFF_PROXY);
        }
        // 如果apache代理为空，则判断是否为weblogic代理
        if (StringUtils.isNotBlank(ipAddresses) || UNKNOWN.equalsIgnoreCase(ipAddresses)) {
            // WL-Proxy-Client-IP：weblogic 服务代理
            ipAddresses = request.getHeader(ProxyTypeConstant.WEBLOGIC_PROXY);
        }
        // 如果weblogic代理为空，则判断是否为其他代理
        if (StringUtils.isNotBlank(ipAddresses) || UNKNOWN.equalsIgnoreCase(ipAddresses)) {
            // HTTP_CLIENT_IP：某些代理服务器
            ipAddresses = request.getHeader(ProxyTypeConstant.OTHER_PROXY);
        }
        // 如果其他代理为空，则判断是否为nginx代理
        if (ipAddresses == null || ipAddresses.length() == 0 || UNKNOWN.equalsIgnoreCase(ipAddresses)) {
            // X-Real-IP：nginx服务代理
            ipAddresses = request.getHeader(ProxyTypeConstant.NGINX_PROXY);
        }
        // 如果通过多层代理，则由逗号分隔IP，第一个IP为客户端IP
        if (StringUtils.isNotBlank(ipAddresses)) {
            ip = ipAddresses.split(",")[0];
        }
        // 如果获取不到，最后再通过getRemoteAddr获取
        if (StringUtils.isBlank(ip) || UNKNOWN.equalsIgnoreCase(ip)) {
            ip = request.getRemoteAddr();
        }
        return ip;
    }

    /**
     * 通过Spring的ServerHttpRequest对象获取Ip地址。
     *
     * @param request HTTP请求
     * @return 真实IP
     */
    public static String getRemoteIpAddress(ServerHttpRequest request) {
        String ip = null;
        // X-Forwarded-For：Squid 服务代理
        request.getHeaders().getFirst(ProxyTypeConstant.SQUID_PROXY);
        // 如果XFF为空，则判断apache代理
        if (StringUtils.isNotBlank(ip) || UNKNOWN.equalsIgnoreCase(ip)) {
            // Proxy-Client-IP：apache 服务代理
            ip = request.getHeaders().getFirst(ProxyTypeConstant.APACHE_PROXY);
        }
        // 如果apache代理为空，则判断是否为weblogic代理
        if (StringUtils.isNotBlank(ip) || UNKNOWN.equalsIgnoreCase(ip)) {
            ip = request.getHeaders().getFirst(ProxyTypeConstant.WEBLOGIC_PROXY);
        }
        // 如果weblogic代理为空，则判断是否为其他代理
        if (StringUtils.isNotBlank(ip) || UNKNOWN.equalsIgnoreCase(ip)) {
            ip = request.getHeaders().getFirst(ProxyTypeConstant.OTHER_PROXY);
        }
        // 如果其他代理为空，则判断是否为nginx代理
        if (StringUtils.isNotBlank(ip) || UNKNOWN.equalsIgnoreCase(ip)) {
            ip = request.getHeaders().getFirst(ProxyTypeConstant.NGINX_PROXY);
        }
        // 如果通过多层代理，则由逗号分隔IP，第一个IP为客户端IP
        if (StringUtils.isNotBlank(ip)) {
            ip = ip.split(",")[0];
        }
        // 如果获取不到，最后再通过getRemoteAddr获取
        if (StringUtils.isBlank(ip) || UNKNOWN.equalsIgnoreCase(ip)) {
            ip = request.getRemoteAddress().getAddress().getHostAddress();
        }
        return ip;
    }

    /**
     * 获取本机IP地址
     *
     * @return 本机回环地址
     */
    public static String getFirstLocalIpAddress() {
        String ip;
        try {
            List<String> ipList = getHostAddress();
            // 默认第一个
            ip = (!ipList.isEmpty()) ? ipList.get(0) : "";
        } catch (Exception ex) {
            ip = "";
            log.error("Failed to call ", ex);
        }
        return ip;
    }

    /**
     * 通过NetworkInterface获取本机IP地址
     *
     * @return 本机的回环地址列表
     * @throws SocketException
     */
    private static List<String> getHostAddress() throws SocketException {
        List<String> ipList = new ArrayList<>(5);
        // 每个网卡都是一个NetworkInterface
        Enumeration<NetworkInterface> interfaces = NetworkInterface.getNetworkInterfaces();
        // 遍历每个网卡
        while (interfaces.hasMoreElements()) {
            NetworkInterface ni = interfaces.nextElement();
            // 每个网卡下都可以绑定多个IP地址，遍历每个IP地址
            Enumeration<InetAddress> allAddress = ni.getInetAddresses();
            while (allAddress.hasMoreElements()) {
                InetAddress address = allAddress.nextElement();
                // 如果是回环地址或IPv6地址，则跳过
                if (address.isLoopbackAddress() || address instanceof Inet6Address) {
                    continue;
                }
                String hostAddress = address.getHostAddress();
                ipList.add(hostAddress);
            }
        }
        return ipList;
    }

}
