package com.lt.common.core.exception;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 自定义Redisson类型异常
 *
 * @Author ToneyMa
 * @Date 2023-04-10 21:17:22
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class InvalidRedisModeException extends RuntimeException{
    private final String mode;

    /**
     * 构造函数。
     *
     * @param mode 错误的模式。
     */
    public InvalidRedisModeException(String mode) {
        super("非法mode参数： [" + mode + "], 仅支持以下几种Redis模式： [single/cluster/sentinel/master-slave] 请检查配置文件。");
        this.mode = mode;
    }
}
