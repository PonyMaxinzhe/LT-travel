package com.lt.common.core.base.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;
import java.util.Map;

/**
 * 数据访问对象的基类。
 *
 * @param <M> 实体类
 * @Author ToneyMa
 * @Date 2023-02-26 21:14:05
 */
public interface BaseDaoMapper<M> extends BaseMapper<M> {
    @Select("<script>" +
            "SELECT ${selectFields} FROM ${selectTable}" +
            "<where>" +
            "   <if test=\"whereClause!=null and whereClause!=''\">" +
            "       AND ${whereClause}" +
            "   </if>" +
            "</where>" +
            "<if test=\"groupBy!=null and groupBy!=''\">" +
            "   GROUP BY ${groupBy}" +
            "</if>" +
            "</script>")
    List<Map<String, Object>> getGroupListByCondition(
            @Param("selectTable") String tableName,
            @Param("selectFields") String tableField,
            @Param("whereClause") String whereClause,
            @Param("groupBy") String groupBy
    );

    @Select("<script>" +
            "SELECT ${selectFields} FROM ${selectTable} " +
            "<where>" +
            "   <if test=\"whereClause!=null and whereClause!=''\">" +
            "       AND ${whereClause} " +
            "   </if>" +
            "</where>" +
            "<if test=\"groupBy!=null and groupBy!=''\">" +
            "   GROUP BY ${orderBy} " +
            "</if>" +
            "</script>")
    List<Map<String, Object>> getListByCondition(
            @Param("selectTable") String tableName,
            @Param("selectFields") String tableField,
            @Param("whereClause") String whereClause,
            @Param("orderBy") String orderBy
    );

    @Select("<script>" +
            "SELECT COUNT(1) FROM ${selectTable} " +
            "<where>" +
            "   <if test=\"whereClause!=null and whereClause!=''\">" +
            "       AND ${whereClause} " +
            "   </if>" +
            "</where>" +
            "</script>")
    int getCountByCondition(@Param("selectTable") String selectTable, @Param("whereClause") String whereClause);
}
