package com.lt.common.core.base.model;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableLogic;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.Date;

/**
 * 实体对象的公共基类，所有子类均必须包含基类定义的数据表字段和实体对象字段。
 *
 * @Author ToneyMa
 * @Date 2023-2-23
 */
@Data
public class BaseModel {

    /**
     * 创建者Id。
     */
    @TableField(value = "create_user")
    private Long createUser;

    /**
     * 创建时间。
     */
    @TableField(value = "create_time")
    private LocalDateTime createTime;

    /**
     * 更新者Id。
     */
    @TableField(value = "update_user")
    private Long updateUser;

    /**
     * 更新时间。
     */
    @TableField(value = "update_time")
    private LocalDateTime updateTime;

    /**
     * 逻辑删除标记字段(1: 正常 -1: 已删除)。
     */
    @TableLogic
    private Integer isDeleted;

}
