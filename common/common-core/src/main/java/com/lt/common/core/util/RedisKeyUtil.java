package com.lt.common.core.util;

/**
 * RedisKey生成工具类
 *
 * @Author ToneyMa
 * @Date 2023-11-23
 */
public class RedisKeyUtil {

    /**
     * 生成当前Session在Redis中存储用的Key
     *
     * @param sessionId
     * @return
     */
    public static String makeSessionIdKey(String sessionId) {
        return "SESSIONID:" + sessionId;
    }

    public static String makeSessionPermIdKey(String sessionId) {
        return "PERM:" + sessionId;
    }
}
