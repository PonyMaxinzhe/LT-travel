package com.lt.common.core.base.service;

import cn.hutool.core.util.ReflectUtil;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lt.common.core.base.dao.BaseDaoMapper;

import java.io.Serializable;
import java.util.function.BiConsumer;
import java.util.function.Consumer;

/**
 * 所有Service的基类
 *
 * @param <M> Model对象的类型
 * @param <K> Model对象主键的类型
 * @Author ToneyMa
 * @Date 2023-02-26 21:09:31
 */
public abstract class BaseService<M, K extends Serializable> extends ServiceImpl<BaseDaoMapper<M>, M> implements IBaseService<M, K> {
    /**
     * 当前Service关联的主Model对象主键字段名称
     */
    protected String idFieldName;

    @Override
    public void saveNewOrUpdate(M data, Consumer<M> saveNew, BiConsumer<M, M> update) {
        if (data == null) {
            return;
        }
        K id = (K) ReflectUtil.getFieldValue(data, idFieldName);
        if (id == null) {
            saveNew.accept(data);
        } else {
            update.accept(data, this.getById(id));
        }
    }
}
