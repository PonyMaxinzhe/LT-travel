package com.lt.common.core.constant;


import cn.hutool.http.Method;

/**
 * 外部API接口地址枚举
 *
 * @Author: ToneyMa
 * @Date: 2023-05-30 16:30
 **/
public enum ApiUrlEnum {
    AMAP_GEOCODE("高德地图-地理编码", "https://restapi.amap.com/v3/geocode/geo", Method.GET),
    AMAP_REGEOCODE("高德地图-逆地理编码", "https://restapi.amap.com/v3/geocode/regeo", Method.GET),
    ;
    private final String description;
    private final String url;
    private final Method requestType;

    /**
     * 构造函数
     *
     * @param url API接口地址
     */
    ApiUrlEnum(String description, String url, Method requestType) {
        this.description = description;
        this.url = url;
        this.requestType = requestType;
    }


    /**
     * 获取对应的URL。
     *
     * @return 对应的URL。
     */
    public String getUrl() {
        return url;
    }

    /**
     * 获取描述信息。
     *
     * @return 描述信息。
     */
    public String getDescription() {
        return description;
    }

    /**
     * 获取请求类型。
     *
     * @return 请求类型。
     */
    public Method getRequestyType() {
        return requestType;
    }
}
