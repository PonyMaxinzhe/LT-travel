package com.lt.common.core.config;

import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import org.apache.ibatis.reflection.MetaObject;
import org.springframework.stereotype.Component;

/**
 * 插入、更新时自动填充字段的处理器。
 *
 * @Author: ToneyMa
 * @Date: 2023-06-27 16:20
 **/
@Component
public class InsertAndUpdateMybatisHandler implements MetaObjectHandler {
    @Override
    public void insertFill(MetaObject metaObject) {

    }

    @Override
    public void updateFill(MetaObject metaObject) {

    }
}
