package com.lt.common.core.constant;

/**
 * 应用程序公共常量
 *
 * @Author: ToneyMa
 * @Date: 2023-07-06 11:30
 **/

public final class ApplicationConstant {

    /**
     * 公共xxx的数据源类型。仅当前服务为多数据源时使用。
     * 在自定义数据源模块中，使用@MyDataSource注解一定要使用该参数。
     * 在多数据源的业务服务中，DataSourceType的常量一定要包含该值，多数据源的配置中，也一定要有与该值匹配的数据源Bean。
     */
    public final static int OPERATION_LOG_DATA_SOURCE = 1000;

    public static final String HTTP_HEADER_TRACE_ID = "traceId";
}
