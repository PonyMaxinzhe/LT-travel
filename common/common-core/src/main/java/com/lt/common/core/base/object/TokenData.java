package com.lt.common.core.base.object;

import com.alibaba.fastjson.JSON;
import com.lt.common.core.util.ContextUtil;
import lombok.Data;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

import javax.servlet.http.HttpServletRequest;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.stream.Collectors;

/**
 * 基于Jwt，用于前后端传递的令牌对象。
 *
 * @author 孙先生
 * @date 2022-01-11
 */
@Data
@ToString
@Slf4j
public class TokenData {

    /**
     * 在HTTP Request对象中的属性键。
     */
    public static final String REQUEST_ATTRIBUTE_NAME = "tokenData";
    /**
     * 用户Id。
     */
    private Long userId;
    /**
     * 用户所属角色。多个角色之间逗号分隔。
     */
    private String roleIds;
    /**
     * 用户所在部门Id。
     * 仅当系统支持uaa时可用，否则可以直接忽略该字段。保留该字段是为了保持单体和微服务通用代码部分的兼容性。
     */
    private String deptId;
    /**
     * 用户所在部门Id集合
     */
    private Set<Long> deptIds;
    /**
     * 用户所属岗位Id。多个岗位之间逗号分隔。仅当系统支持岗位时有值。
     */
    private String postIds;
    /**
     * 用户的部门岗位Id。多个岗位之间逗号分隔。仅当系统支持岗位时有值。
     */
    private String deptPostIds;
    /**
     * 是否为超级管理员。
     */
    private Boolean isAdmin;
    /**
     * 用户登录名。
     */
    private String loginName;
    /**
     * 用户显示名称。
     */
    private String showName;
    /**
     * 用户类型,工作人员也是网格员时存1,2(0: 管理员 1: 工作人员)。
     */
    private String userType;
    /**
     * 联系方式
     */
    private String phone;
    /**
     * 设备类型。参考 AppDeviceType。
     */
    private Integer deviceType;
    /**
     * 标识不同登录的会话Id。
     */
    private String sessionId;
    /**
     * 数据库路由键(仅当水平分库时使用)。
     */
    private Integer datasourceRouteKey;
    /**
     * 登录IP。
     */
    private String loginIp;
    /**
     * 登录时间。
     */
    private Date loginTime;
    /**
     * 登录头像地址。
     */
    private String headImageUrl;

    public void setDeptId(String deptId) {
        this.deptId = deptId;
        this.deptIds = StringUtils.isEmpty(deptId) ? Collections.emptySet() :
                Arrays.stream(deptId.split(",")).map(Long::valueOf).collect(Collectors.toSet());
    }

    private static final TokenData NO_LOGIN_USER;

    static {
        NO_LOGIN_USER = new TokenData();
        NO_LOGIN_USER.userId = 1480746425070391296L;
        NO_LOGIN_USER.loginName = "admin";
        NO_LOGIN_USER.showName = "管理员";
        NO_LOGIN_USER.isAdmin = true;
        NO_LOGIN_USER.deptId = "1480746425137500162";
        NO_LOGIN_USER.deptIds = new HashSet<>(1);
        NO_LOGIN_USER.deptIds.add(1480746425137500162L);
    }

    /**
     * 将令牌对象添加到Http请求对象。
     *
     * @param tokenData 令牌对象。
     */
    public static void addToRequest(TokenData tokenData) {
        HttpServletRequest request = ContextUtil.getHttpRequest();
        request.setAttribute(TokenData.REQUEST_ATTRIBUTE_NAME, tokenData);
    }

    /**
     * 从Http Request对象中获取令牌对象。
     *
     * @return 令牌对象。
     */
    public static TokenData takeFromRequest() {
        HttpServletRequest request = ContextUtil.getHttpRequest();
        TokenData tokenData = (TokenData) request.getAttribute(REQUEST_ATTRIBUTE_NAME);
        if (tokenData != null) {
            return tokenData;
        }
        String token = request.getHeader(REQUEST_ATTRIBUTE_NAME);
        if (StringUtils.isNotBlank(token)) {
            tokenData = JSON.parseObject(token, TokenData.class);
        } else {
            token = request.getParameter(REQUEST_ATTRIBUTE_NAME);
            if (StringUtils.isNotBlank(token)) {
                tokenData = JSON.parseObject(token, TokenData.class);
            }
        }
        if (tokenData != null) {
            try {
                tokenData.showName = URLDecoder.decode(tokenData.showName, StandardCharsets.UTF_8.name());
            } catch (UnsupportedEncodingException e) {
                log.error("Failed to call TokenData.takeFromRequest", e);
            }
            addToRequest(tokenData);
        }
        return tokenData;
    }

    /**
     * 当没有登录，但又想要登录用户信息，默认admin信息，字段信息不全，慎用！！！
     */
    public static TokenData getNoLoginUser() {
        return NO_LOGIN_USER;
    }
}
