package com.lt.common.core.config;

import feign.RequestInterceptor;
import feign.RequestTemplate;
import lombok.SneakyThrows;
import org.springframework.context.annotation.Configuration;

/**
 * FeignClient配置类。
 *
 * @Author ToneyMa
 * @Date 2023-03-03 22:10:07
 */
@Configuration
public class FeignConfig implements RequestInterceptor {
    @Override
    @SneakyThrows
    public void apply(RequestTemplate requestTemplate) {
//        requestTemplate
    }
}
