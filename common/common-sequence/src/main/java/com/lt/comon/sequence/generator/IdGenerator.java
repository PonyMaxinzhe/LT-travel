package com.lt.comon.sequence.generator;

/**
 * 分布式ID生成器统一接口。
 *
 * @Author ToneyMa
 * @Date 2023-03-31 16:21:43
 */
public interface IdGenerator {
    /**
     * 获取long类型的分布式ID
     * @return
     */
    long nextLongId();

    /**
     * 获取String类型的分布式ID
     * @return
     */
    String nextStringId();
}
