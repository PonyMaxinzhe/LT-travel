package com.lt.comon.sequence.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * 分布式ID生成器配置类
 *
 * @Author ToneyMa
 * @Date 2023-03-30 19:38:48
 */
@Data
@ConfigurationProperties(prefix = "sequence")
public class IdGeneratorProperties {
    /**
     * 是否基于美团Leaf分布式ID生成器。(默认不使用)
     */
    private Boolean advanceIdGenerator = false;
    /**
     * Zookeeper地址，多个地址间，用分号分隔,当advanceIdGenerator为true时生效。
     * 如：192.168.1.1:2181;192.168.1.2:2181;192.168.1.3:2181
     */
    private String zkAddress;
    /**
     * 用于识别同一主机(ip相同)不同服务的端口号。与本机的ip一起构成zk中标识不同服务实例的key值。
     * 仅当advanceIdGenerator为true时生效。
     */
    private Integer idPort;
    /**
     * zk中生成WorkNode的路径。不同的业务可以使用不同的路径，以免冲突。
     * 仅当advanceIdGenerator为true时生效。
     */
    private String zkPath;
}
