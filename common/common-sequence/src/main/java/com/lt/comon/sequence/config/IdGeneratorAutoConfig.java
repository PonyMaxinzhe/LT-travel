package com.lt.comon.sequence.config;


import org.springframework.boot.context.properties.EnableConfigurationProperties;

/**
 * common-sequence模块的自动配置引导类。
 *
 * @Author ToneyMa
 * @Date 2023-03-30 19:39:05
 */
@EnableConfigurationProperties({IdGeneratorProperties.class})
public class IdGeneratorAutoConfig {
}
