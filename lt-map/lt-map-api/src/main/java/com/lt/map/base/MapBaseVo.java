package com.lt.map.base;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 高德地图基础VO对象
 *
 * @Author: ToneyMa
 * @Date: 2023-05-31 15:25
 **/
@Data
@NoArgsConstructor
@AllArgsConstructor
public class MapBaseVo {
    /**
     * 返回状态
     * 1：成功；0：失败
     */
    private Integer status;

    /**
     * 返回的状态信息
     * status为0时，info返回错误原；否则返回“OK”。详情参阅info状态表
     */
    private String info;
}
