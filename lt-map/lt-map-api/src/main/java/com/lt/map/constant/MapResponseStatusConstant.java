package com.lt.map.constant;

/**
 * 外部接口返回状态常量类
 *
 * @Author: ToneyMa
 * @Date: 2023-05-31 15:29
 **/

public final class MapResponseStatusConstant {
    /**
     * 成功
     */
    public static final Integer FAIL = 0;

    /**
     * 失败
     */
    public static final Integer SUCCESS = 1;
}
