package com.lt.map.vo;

import com.lt.map.base.MapBaseVo;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

/**
 * 地图编码服务Vo对象
 *
 * @Author: ToneyMa
 * @Date: 2023-05-30 17:52
 **/
@Data
@EqualsAndHashCode(callSuper = true)
public class GeoCodeMapVo extends MapBaseVo {

    /**
     * 返回结果数目
     */
    private Long count;

    /**
     * 返回地址信息列表
     */
    private List<GeoCodeInfoVo> geocodes;
}
