package com.lt.map.config;

import com.lt.common.core.constant.ApiUrlEnum;
import feign.RequestInterceptor;
import feign.RequestTemplate;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * 地图服务Feign配置类
 *
 * @Author: ToneyMa
 * @Date: 2023-05-31 14:07
 **/
@Configuration
@Slf4j
public class MapApiHttpConfig implements RequestInterceptor {

    @Value("${map.api.key}")
    private String key;

    @Override
    public void apply(RequestTemplate requestTemplate) {
        // 仅处理GET请求
        String requestType = requestTemplate.method();
        if (RequestMethod.GET.name().equals(requestType)) {
            // 添加请求参数
            requestTemplate.query("key", key);
            // 返回格式
            requestTemplate.query("output", "JSON");
            log.info("请求地址：{}", requestTemplate.url());
        }
    }
}
