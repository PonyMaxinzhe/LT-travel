package com.lt.map.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * 周边建筑Vo
 *
 * @Author: ToneyMa
 * @Date: 2023-06-02 16:57
 **/

@Data
@NoArgsConstructor
@AllArgsConstructor
public class BuildingVo {
    /**
     * 社区名称
     */
    private List<String> name;

    /**
     * 社区类型
     */
    private List<String> type;
}
