package com.lt.map.vo;

import lombok.Data;

import java.util.List;

/**
 * 地理编码信息
 *
 * @Author: ToneyMa
 * @Date: 2023-05-30 17:56
 **/
@Data
public class GeoCodeInfoVo {

    /**
     * 格式化地址
     */
    private String formatted_address;

    /**
     * 国家
     * 国内地址默认返回中国
     */
    private String country;

    /**
     * 地址所在的省份名
     * 例如：北京市。此处需要注意的是，中国的四大直辖市也算作省级单位。
     */
    private String province;

    /**
     * 地址所在的城市名
     * 例如：北京市
     */
    private String city;

    /**
     * 城市编码
     * 例如：010
     */
    private String citycode;

    /**
     * 地址所在的区
     * 例如：朝阳区
     */
    private List<String> district;

    /**
     * 村镇关系
     */
    private List<String> township;

    /**
     * 街道列表
     * 例如：阜通东大街
     */
    private List<String> street;

    /**
     * 门牌列表
     * 例如：6号
     */
    private List<String> number;

    /**
     * 区域编码
     * 例如：110101
     */
    private String adcode;

    /**
     * 坐标点
     * 经度，纬度
     */
    private String location;

    /**
     * 匹配级别
     * 参见下方的地理编码匹配级别列表
     */
    private String level;

    /**
     * 邻居/周边信息
     */
    private NeighborhoodVo neighborhood;

    /**
     * 周边建筑信息
     */
    private BuildingVo building;
}
