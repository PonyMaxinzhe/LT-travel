package com.lt.map.client;

import com.lt.common.core.constant.amap.MapStatusContant;
import com.lt.map.config.MapApiHttpConfig;
import com.lt.map.vo.GeoCodeMapVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.openfeign.FallbackFactory;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * 声明式调用高德地图服务接口
 *
 * @Author: ToneyMa
 * @Date: 2023-05-31 14:05
 **/
@FeignClient(
        name = "amap-service",
        url = "https://restapi.amap.com/v3",
        configuration = MapApiHttpConfig.class,
        fallbackFactory = MapApiHttpClient.MapApiHttpClientFallbackFactory.class
)
public interface MapApiHttpClient {

    @GetMapping("/geocode/geo")
    GeoCodeMapVo geoCode(@RequestParam("address") String address, @RequestParam("city") String city);

    @Component("MapApiHttpClientFallbackFactory")
    @Slf4j
    class MapApiHttpClientFallbackFactory
            implements FallbackFactory<MapApiHttpClient>, MapApiHttpClient {
        @Override
        public GeoCodeMapVo geoCode(String address, String city) {
            log.error("Failed to Feign Remote call [geoCode]");
            // 构造失败响应
            GeoCodeMapVo vo = new GeoCodeMapVo();
            vo.setStatus(MapStatusContant.FAIL);
            return vo;
        }

        @Override
        public MapApiHttpClient create(Throwable throwable) {
            log.error("Exception For Feign Remote Call.", throwable);
            return new MapApiHttpClientFallbackFactory();
        }
    }
}
