import java.util.ArrayList;
import java.util.List;

/**
 * @Author: ToneyMa
 * @Date: 2023-06-28 16:28
 **/

public class Test {
    public static void main(String[] args) {
        List<Integer> a = new ArrayList<>();
        List<Integer> b = new ArrayList<>();
        List<Integer> result = new ArrayList<>();

        get(b, 0, a.get(0), result);
    }

    private static void get(List<Integer> b, int bStartIndex, Integer temp, List<Integer> result) {
        // 从bStartIndex开始遍历b
        for (int i = bStartIndex; i < b.size(); i++) {
            // 如果有相同的，就从a的下一个开始遍历
            if (temp.equals(b.get(i))) {
                // 重置起始下标
                bStartIndex = i + 1;


                // 在这里生成新的temp
//                temp = xxxxxxx;


                // 覆盖b对应的记录
                b.set(i, temp);
                // 保存到结果集
                result.add(temp);
                // 递归调用
                get(b, bStartIndex, temp, result);
                break;
            }
        }
    }
}
