package com.lt.map;

import feign.Logger;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;

/**
 * 地图服务启动类
 *
 * @Author: ToneyMa
 * @Date: 2023-05-30 11:22
 **/
@SpringBootApplication
@EnableFeignClients(basePackages = "com.lt")
@EnableDiscoveryClient
@ComponentScan("com.lt")
public class MapApplication {
    public static void main(String[] args) {
        SpringApplication.run(MapApplication.class, args);
    }

    @Bean
    public Logger.Level feignLoggerLevel() {
        return Logger.Level.FULL;
    }
}
