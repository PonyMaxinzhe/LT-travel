package com.lt.map.controller;

import cn.hutool.core.bean.BeanUtil;
import com.lt.common.core.base.object.ResponseResult;
import com.lt.common.core.constant.ErrorCodeEnum;
import com.lt.common.core.util.MyCommonUtil;
import com.lt.map.constant.MapResponseStatusConstant;
import com.lt.map.dto.GeoCodeDto;
import com.lt.map.service.MapService;
import com.lt.map.vo.GeoCodeMapVo;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * 地图服务Controller
 *
 * @Author: ToneyMa
 * @Date: 2023-05-30 13:35
 **/
@RestController
@RequestMapping("/map")
public class MapController {

    @Resource
    private MapService mapService;

    @PostMapping("/geoCode")
    public ResponseResult<GeoCodeMapVo> geoCode(@RequestBody GeoCodeDto geoCodeDto) {
        String errorMessage = MyCommonUtil.getModelValidationError(geoCodeDto, false);
        if (errorMessage != null) {
            return ResponseResult.error(ErrorCodeEnum.DATA_VALIDATED_FAILED, errorMessage);
        }
        GeoCodeMapVo vo = mapService.geoCode(geoCodeDto);
        return checkApiResponseStatus(vo);
    }

    private ResponseResult<GeoCodeMapVo> checkApiResponseStatus(GeoCodeMapVo vo) {
        if (BeanUtil.isEmpty(vo)) {
            return ResponseResult.error(ErrorCodeEnum.RPC_DATA_ACCESS_FAILED);
        }
        // 如果返回状态为失败，则返回错误信息
        if (MapResponseStatusConstant.FAIL.equals(vo.getStatus())) {
            return ResponseResult.error(ErrorCodeEnum.RPC_DATA_ACCESS_FAILED, vo.getInfo());
        }
        return ResponseResult.success(vo);
    }
}
