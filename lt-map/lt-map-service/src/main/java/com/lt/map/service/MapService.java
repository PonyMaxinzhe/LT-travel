package com.lt.map.service;

import com.lt.map.dto.GeoCodeDto;
import com.lt.map.vo.GeoCodeMapVo;

/**
 * 地图服务接口
 *
 * @Author: ToneyMa
 * @Date: 2023-05-31 11:05
 **/

public interface MapService {
    GeoCodeMapVo geoCode(GeoCodeDto geoCodeDto);
}
