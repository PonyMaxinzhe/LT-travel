package com.lt.map.service.impl;

import com.lt.map.client.MapApiHttpClient;
import com.lt.map.dto.GeoCodeDto;
import com.lt.map.service.MapService;
import com.lt.map.vo.GeoCodeMapVo;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * 地图服务接口实现类
 *
 * @Author: ToneyMa
 * @Date: 2023-05-31 11:06
 **/
@Service
public class MapServiceImpl implements MapService {

    @Resource
    private MapApiHttpClient mapApiHttpClient;

    @Override
    public GeoCodeMapVo geoCode(GeoCodeDto geoCodeDto) {
        System.out.println("Feign Client Class: " + mapApiHttpClient.getClass().getName());
        return mapApiHttpClient.geoCode(geoCodeDto.getAddress(), geoCodeDto.getCity());
    }
}
