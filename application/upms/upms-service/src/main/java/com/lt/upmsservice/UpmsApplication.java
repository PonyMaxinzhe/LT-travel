package com.lt.upmsservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Component;

/**
 * UPMS服务启动类
 *
 * @Author ToneyMa
 */
@ComponentScan("com.lt")
@EnableDiscoveryClient
@EnableFeignClients(basePackages = {"com.lt"})
@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class})
public class UpmsApplication {
    public static void main(String[] args) {
        SpringApplication.run(UpmsApplication.class, args);
    }
}