package com.lt.upmsservice.model;

import com.baomidou.mybatisplus.annotation.TableName;
import com.lt.common.core.base.model.BaseModel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 用户 角色关联表
 *
 * @Author: ToneyMa
 * @Date: 2023-06-27 16:06
 **/
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("sys_user_role_relation")
public class SysUserRoleRelation extends BaseModel implements Serializable {

    /**
     * 用户主键ID
     */
    private Long userId;

    /**
     * 角色主键ID
     */
    private Long roleId;
}