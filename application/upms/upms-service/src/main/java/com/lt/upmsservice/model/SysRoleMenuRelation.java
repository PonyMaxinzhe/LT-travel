package com.lt.upmsservice.model;

import com.baomidou.mybatisplus.annotation.TableName;
import com.lt.common.core.base.model.BaseModel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 角色 菜单关联表
 *
 * @Author: ToneyMa
 * @Date: 2023-06-27 16:08
 **/
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("sys_role_menu_relation")
public class SysRoleMenuRelation extends BaseModel implements Serializable {

    /**
     * 角色主键ID
     */
    private Long roleId;

    /**
     * 菜单主键ID
     */
    private Long menuId;
}