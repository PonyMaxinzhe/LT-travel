package com.lt.upmsservice.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lt.upmsservice.dao.SysUserMapper;
import com.lt.upmsservice.model.SysUser;
import com.lt.upmsservice.service.SysUserService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * 系统用户Service实现类。
 *
 * @Author: ToneyMa
 * @Date: 2023-07-06 15:48
 **/
@Service
public class SysUserServiceImpl extends ServiceImpl<SysUserMapper, SysUser> implements SysUserService {

    @Resource
    private SysUserMapper sysUserMapper;

    @Override
    public SysUser getUserInfo(Long id) {
        return sysUserMapper.selectById(id);
    }
}
