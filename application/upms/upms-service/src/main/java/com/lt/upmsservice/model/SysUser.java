package com.lt.upmsservice.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.lt.common.core.base.model.BaseModel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 用户表
 *
 * @Author: ToneyMa
 * @Date: 2023-06-27 15:51
 **/
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("sys_user")
public class SysUser extends BaseModel implements Serializable {

    /**
     * 用户主键ID
     */
    @TableId(value = "user_id", type = IdType.AUTO)
    private Long userId;

    /**
     * 登录用户名
     */
    private String loginName;

    /**
     * 显示用户名（昵称）
     */
    private String showName;

    /**
     * 性别字典键ID（KEY_CODE：GENDER_TYPE）
     */
    private Long genderTypeDict;

    /**
     * 部门主键ID
     */
    private Long deptId;

    /**
     * 电话号码
     */
    private String userTel;
}
