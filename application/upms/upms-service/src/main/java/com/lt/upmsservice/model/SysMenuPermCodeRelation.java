package com.lt.upmsservice.model;

import com.baomidou.mybatisplus.annotation.TableName;
import com.lt.common.core.base.model.BaseModel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 菜单 权限字关联表
 *
 * @Author: ToneyMa
 * @Date: 2023-06-27 16:12
 **/
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("sys_menu_perm_code_relation")
public class SysMenuPermCodeRelation extends BaseModel implements Serializable {

    /**
     * 菜单主键ID
     */
    private Long menuId;

    /**
     * 权限字主键ID
     */
    private Long permCodeId;
}
