package com.lt.upmsservice.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.lt.common.core.base.model.BaseModel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 权限字
 *
 * @Author: ToneyMa
 * @Date: 2023-06-27 16:02
 **/
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("sys_perm_code")
public class SysPermCode extends BaseModel implements Serializable {

    /**
     * 权限字主键ID
     */
    @TableId(value = "perm_code_id", type = IdType.AUTO)
    private Long permCodeId;

    /**
     * 权限字
     */
    private String permCode;
}
