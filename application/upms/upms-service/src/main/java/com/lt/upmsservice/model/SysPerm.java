package com.lt.upmsservice.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.lt.common.core.base.model.BaseModel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 权限资源
 *
 * @Author: ToneyMa
 * @Date: 2023-06-27 16:03
 **/
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("sys_perm")
public class SysPerm extends BaseModel implements Serializable {

    /**
     * 权限资源主键ID
     */
    @TableId(value = "perm_id", type = IdType.AUTO)
    private Long permId;

    /**
     * 权限资源名
     */
    private String permName;

    /**
     * 权限资源对应的URL
     */
    private String url;
}