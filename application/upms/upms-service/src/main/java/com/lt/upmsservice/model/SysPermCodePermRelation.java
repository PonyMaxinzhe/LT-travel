package com.lt.upmsservice.model;

import com.baomidou.mybatisplus.annotation.TableName;
import com.lt.common.core.base.model.BaseModel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 权限字 权限资源关联表
 *
 * @Author: ToneyMa
 * @Date: 2023-06-27 16:13
 **/

@Data
@EqualsAndHashCode
@Accessors(chain = true)
@TableName("sys_perm_code_perm_relation")
public class SysPermCodePermRelation extends BaseModel implements Serializable {

    /**
     * 权限字主键ID
     */
    private Long permCodeId;

    /**
     * 权限资源主键ID
     */
    private Long permCode;
}