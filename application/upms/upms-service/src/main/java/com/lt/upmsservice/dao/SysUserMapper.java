package com.lt.upmsservice.dao;

import com.lt.common.core.base.dao.BaseDaoMapper;
import com.lt.upmsservice.model.SysUser;

/**
 * 系统用户Mapper类。
 *
 * @Author: ToneyMa
 * @Date: 2023-07-06 15:48
 **/
public interface SysUserMapper extends BaseDaoMapper<SysUser> {
}
