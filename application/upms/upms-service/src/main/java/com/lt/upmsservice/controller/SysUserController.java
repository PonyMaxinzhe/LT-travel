package com.lt.upmsservice.controller;

import com.lt.upmsservice.model.SysUser;
import com.lt.upmsservice.service.SysUserService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * 系统用户Controller类。
 *
 * @Author: ToneyMa
 * @Date: 2023-07-06 15:45
 **/
@RestController
@RequestMapping("/sysUser")
public class SysUserController {

    @Resource
    private SysUserService sysUserService;

    @GetMapping("/getById")
    public SysUser getById(@RequestParam Long id) {
        return sysUserService.getUserInfo(id);
    }

}
