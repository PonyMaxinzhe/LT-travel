package com.lt.upmsservice.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lt.upmsservice.model.SysUser;

/**
 * 系统用户Service接口。
 *
 * @Author: ToneyMa
 * @Date: 2023-07-06 15:47
 **/

public interface SysUserService extends IService<SysUser> {
    SysUser getUserInfo(Long id);
}
