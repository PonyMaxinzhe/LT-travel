package com.lt.upmsservice.config;

import com.alibaba.druid.spring.boot.autoconfigure.DruidDataSourceBuilder;
import com.lt.common.core.config.DynamicDataSource;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;

/**
 * 多数据源配置对象。
 *
 * @Author: ToneyMa
 * @Date: 2023-07-06 15:26
 **/
@Configuration
@EnableTransactionManagement
@MapperScan(value = {"com.lt.*.dao", "com.lt.common.*.dao"})
public class MultiDataSourceConfig {
    @Bean(initMethod = "init", destroyMethod = "close")
    @ConfigurationProperties(prefix = "spring.datasource.druid.upms-admin")
    public DataSource upmsAdminDataSource() {
        return DruidDataSourceBuilder.create().build();
    }

    @Bean(initMethod = "init", destroyMethod = "close")
    @ConfigurationProperties(prefix = "spring.datasource.druid.operation-log")
    public DataSource operationLogDataSource() {
        return DruidDataSourceBuilder.create().build();
    }

    @Bean
    @Primary
    public DynamicDataSource dataSource() {
        Map<Object, Object> targetDataSources = new HashMap<>(1);
        targetDataSources.put(DataSourceType.UPMS_ADMIN, upmsAdminDataSource());
        targetDataSources.put(DataSourceType.OPERATION_LOG_DATA_SOURCE, operationLogDataSource());
        // 请务必保证业务数据表所在数据库为缺省数据源。
        DynamicDataSource dynamicDataSource = new DynamicDataSource();
        dynamicDataSource.setTargetDataSources(targetDataSources);
        dynamicDataSource.setDefaultTargetDataSource(upmsAdminDataSource());
        return dynamicDataSource;
    }
}
