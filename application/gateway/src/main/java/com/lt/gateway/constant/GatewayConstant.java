package com.lt.gateway.constant;

/**
 * GatewayConstant，网关业务常量。
 *
 * @Author ToneyMa
 * @Date 2023-09-28
 **/
public final class GatewayConstant {

    /**
     * 请求进入网关的开始时间。
     */
    public static final String START_TIME_ATTRIBUTE = "startTime";

    /**
     * 登录URL。
     */
    public static final String ADMIN_LOGIN_URL = "/admin/upms/login/doLogin";

    /**
     * 获取短信验证码URL。
     */
    public static final String ADMIN_VERIFICATION_CODE_URL = "/admin/upms/login/verificationCode";
    /**
     * 校验短信验证码URL。
     */
    public static final String ADMIN_CHECK_VERIFICATION_CODE_URL = "/admin/upms/login/checkVerificationCode";

    /**
     * 手机号验证码登录URL。
     */
    public static final String ADMIN_LOGIN_BY_PHONE_URL = "/admin/upms/login/doLoginByPhone";

    /**
     * 找回密码URL。
     */
    public static final String ADMIN_FORGET_PASSWORD_URL = "/admin/upms/login/forgetPassword";
    /**
     * 登出URL。
     */
    public static final String ADMIN_LOGOUT_URL = "/admin/upms/login/doLogout";

    /**
     * sessionId的键名称。
     */
    public static final String SESSION_ID_KEY_NAME = "sessionId";

    /**
     * 私有构造函数，明确标识该常量类的作用。
     */
    private GatewayConstant() {
    }
}
