package com.lt.gateway.filter;

import cn.hutool.core.util.ObjectUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.lt.common.core.base.object.ResponseResult;
import com.lt.common.core.base.object.TokenData;
import com.lt.common.core.constant.ErrorCodeEnum;
import com.lt.common.core.util.IpUtil;
import com.lt.common.core.util.JwtUtil;
import com.lt.common.core.util.RedisKeyUtil;
import com.lt.gateway.config.ApplicationConfig;
import com.lt.gateway.constant.GatewayConstant;
import io.jsonwebtoken.Claims;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.redisson.api.RBucket;
import org.redisson.api.RedissonClient;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.core.Ordered;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.util.AntPathMatcher;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import javax.annotation.Resource;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;

/**
 * 全局前端过滤器
 *
 * @Author: ToneyMa
 * @Date: 2023-09-28
 **/
@Slf4j
public class AuthenticationPreFilter implements GlobalFilter, Ordered {

    @Resource
    private ApplicationConfig config;

    @Resource
    private RedissonClient redissonClient;

    /**
     * Ant Pattern模式的白名单地址匹配器。
     */
    private final AntPathMatcher antMatcher = new AntPathMatcher();

    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        // 获取请求对象
        ServerHttpRequest request = exchange.getRequest();
        // 获取响应对象
        ServerHttpResponse response = exchange.getResponse();
        // 获取请求地址
        String url = request.getURI().getPath();
        // 获取token信息
        String token = this.getTokenFromRequest(request);
        // 是否登录标记。
        boolean noNeedLoginUrl = false;
        // 判断是否为白名单请求，以及一些内置不需要验证的请求。(登录请求也包含其中)。
        // 如果当前请求中包含token令牌不为空的时候，也会继续验证Token的合法性，这样就能保证
        // Token中的用户信息被业务接口正常访问到了。而如果当token为空的时候，白名单的接口可以
        // 被网关直接转发，无需登录验证。当然被转发的接口，也无法获取到用户的token身份数据了。
        if (this.shouldNotFilter(url)) {
            noNeedLoginUrl = true;
            if (StringUtils.isBlank(token)) {
                return chain.filter(exchange);
            }
        }
        // 解析令牌
        Claims c = JwtUtil.parseToken(token, config.getTokenSignKey());
        // 判断是否过期或为空
        if (JwtUtil.isNullOrExpired(c)) {
            log.warn("EXPIRED request [{}] from REMOTE-IP [{}].", url, IpUtil.getRemoteIpAddress(request));
            // 设置响应状态码
            response.setStatusCode(HttpStatus.UNAUTHORIZED);
            // 设置响应类型
            response.getHeaders().setContentType(MediaType.APPLICATION_JSON);
            // 响应结果转化为字节数组
            byte[] responseBody = JSON.toJSONString(ResponseResult.error(ErrorCodeEnum.UNAUTHORIZED_LOGIN,
                    "用户登录已过期或尚未登录，请重新登录！")).getBytes(StandardCharsets.UTF_8);
            return response.writeWith(Flux.just(response.bufferFactory().wrap(responseBody)));
        }
        // 这里判断是否需要定时刷新token
        if (JwtUtil.needToRefresh(c)) {
            // 如果需要则把配置好的刷新后的Header中的tokenName和tokenValue设置到响应头中。
            exchange.getAttributes().put(config.getRefreshedTokenHeaderKey(),
                    JwtUtil.generateToken(c, config.getTokenExpiration(), config.getTokenSignKey()));
        }
        // 获取令牌中的sessionId
        String sessionId = (String) c.get(GatewayConstant.SESSION_ID_KEY_NAME);
        // 生成sessionId在Redis中对应的key
        String sessionIdKey = RedisKeyUtil.makeSessionIdKey(sessionId);
        // 获取Redis中的session数据
        RBucket<String> sessionData = redissonClient.getBucket(sessionIdKey);
        JSONObject tokenData = null;
        // 如果session存在，则获取session中的数据
        if (sessionData.isExists()) {
            tokenData = JSONObject.parseObject(sessionData.get());
        }
        // 如果TokenData不存在，则打印日志，并写出错误信息
        if (ObjectUtil.isEmpty(tokenData)) {
            log.warn("UNAUTHORIZED request [{}] from REMOTE-IP [{}] because no sessionId exists in redis.",
                    url, IpUtil.getRemoteIpAddress(request));
            response.setStatusCode(HttpStatus.UNAUTHORIZED);
            response.getHeaders().setContentType(MediaType.APPLICATION_JSON);
            byte[] responseBody = JSON.toJSONString(ResponseResult.error(ErrorCodeEnum.UNAUTHORIZED_LOGIN,
                    "用户会话已失效，请重新登录！")).getBytes(StandardCharsets.UTF_8);
            // 写出响应
            return response.writeWith(Flux.just(response.bufferFactory().wrap(responseBody)));
        }
        // 获取userId
        String userId = tokenData.getString("userId");
        if (StringUtils.isBlank(userId)) {
            log.warn("UNAUTHORIZED request [{}] from REMOTE-IP [{}] because userId is empty in redis.",
                    url, IpUtil.getRemoteIpAddress(request));
            response.setStatusCode(HttpStatus.UNAUTHORIZED);
            response.getHeaders().setContentType(MediaType.APPLICATION_JSON);
            byte[] responseBody = JSON.toJSONString(ResponseResult.error(ErrorCodeEnum.UNAUTHORIZED_LOGIN,
                    "用户登录验证信息已过期，请重新登录！")).getBytes(StandardCharsets.UTF_8);
            return response.writeWith(Flux.just(response.bufferFactory().wrap(responseBody)));
        }
        // 获取昵称
        String showName = tokenData.getString("showName");
        // 因为http header中不支持中文传输，所以需要编码。
        try {
            showName = URLEncoder.encode(showName, StandardCharsets.UTF_8.name());
            tokenData.put("showName", showName);
        } catch (UnsupportedEncodingException e) {
            log.error("Failed to call AuthenticationPreFilter.filter.", e);
        }
        // 判断是否为管理员
        boolean isAdmin = tokenData.getBoolean("isAdmin");

        // 如果不是无需登录的url 并且 不是管理员 并且 对当前url没有权限，则写出无权限错误。
        if (!noNeedLoginUrl && Boolean.FALSE.equals(isAdmin) && !this.hasPermission(redissonClient, sessionId, url)) {
            log.warn("FORBIDDEN request [{}] from REMOTE-IP [{}] for USER [{} -- {}] no perm!",
                    url, IpUtil.getRemoteIpAddress(request), userId, showName);
            response.setStatusCode(HttpStatus.FORBIDDEN);
            response.getHeaders().setContentType(MediaType.APPLICATION_JSON);
            byte[] responseBody = JSON.toJSONString(ResponseResult.error(ErrorCodeEnum.NO_OPERATION_PERMISSION,
                    "用户对该URL没有访问权限，请核对！")).getBytes(StandardCharsets.UTF_8);
            return response.writeWith(Flux.just(response.bufferFactory().wrap(responseBody)));
        }
        // 将session中关联的用户信息，添加到当前的Request中。转发后，业务服务可以根据需要自定读取。
        tokenData.put("sessionId", sessionId);
        exchange.getAttributes().put(GatewayConstant.SESSION_ID_KEY_NAME, sessionId);
        // 这里按照HTTP协议的规定，一旦Http的请求或响应被创建和发送，它就是不可变的，不能被修改。
        // 这是因为HTTP协议是无状态的，每一个请求和响应都是独立的，不能对过去的请求或未来的请求产生副作用。
        // 所以需要在这里用mutate重新构建一个新的请求对象和exchange对象，然后把新的request添加到新构建的Exchange中，然后再放行请求。
        ServerHttpRequest mutableReq = exchange.getRequest()
                .mutate()
                .header(TokenData.REQUEST_ATTRIBUTE_NAME, JSON.toJSONString(tokenData))
                .build();
        ServerWebExchange mutableExchange = exchange
                .mutate()
                .request(mutableReq)
                .build();
        return chain.filter(mutableExchange);
    }

    private boolean hasPermission(RedissonClient redissonClient, String sessionId, String url) {
        // 如果是登出操作则无需校验权限
        if (url.equals(GatewayConstant.ADMIN_LOGOUT_URL)) {
            return true;
        }
        // 获取Perm权限在Redis中的key
        String permKey = RedisKeyUtil.makeSessionPermIdKey(sessionId);
        // 校验url权限
        return redissonClient.getSet(permKey).contains(url);
    }

    /**
     * 判断当前请求URL是否为白名单地址，以及一些内置的不用登录的接口，
     *
     * @param url 请求的url。
     * @return 是返回true，否返回false。
     */
    private boolean shouldNotFilter(String url) {
        // 过滤swagger相关url
        if (url.endsWith("/v2/api-docs") || url.endsWith("/v2/api-docs-ext")) {
            return true;
        }
        if (GatewayConstant.ADMIN_LOGIN_URL.equals(url)
                || GatewayConstant.ADMIN_LOGIN_BY_PHONE_URL.equals(url)
                || GatewayConstant.ADMIN_LOGOUT_URL.equals(url)
                || GatewayConstant.ADMIN_FORGET_PASSWORD_URL.equals(url)
                || GatewayConstant.ADMIN_VERIFICATION_CODE_URL.equals(url)
                || GatewayConstant.ADMIN_CHECK_VERIFICATION_CODE_URL.equals(url)
                || url.startsWith("/captcha")) {
            return true;
        }
        // 先过滤白名单
        if (CollectionUtils.isNotEmpty(config.getWhiteListUrl())) {
            if (config.getWhiteListUrl().contains(url)) {
                return true;
            }
        }
        // 过滤ant pattern模式的白名单url
        if (CollectionUtils.isNotEmpty(config.getWhitelistUrlPattern())) {
            for (String urlPattern : config.getWhitelistUrlPattern()) {
                if (antMatcher.match(urlPattern, url)) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * 从请求中获取token。
     *
     * @param request
     * @return
     */
    private String getTokenFromRequest(ServerHttpRequest request) {
        // 先从请求头获取。
        String token = request.getHeaders().getFirst(config.getTokenHeaderName());
        if (StringUtils.isBlank(token)) {
            // 再到查询参数中获取。
            token = request.getQueryParams().getFirst(config.getTokenHeaderName());
        }
        return token;
    }

    @Override
    public int getOrder() {
        return HIGHEST_PRECEDENCE + 10000;
    }
}
