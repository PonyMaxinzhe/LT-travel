package com.lt.gateway.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.annotation.Configuration;

import java.util.Set;

/**
 * 网关配置类。（支持热更新）
 *
 * @Author ToneyMa
 * @Date 2023-09-27
 **/
@Data
@RefreshScope
@ConfigurationProperties(prefix = "application")
@Configuration
public class ApplicationConfig {

    /**
     * token加密用的秘钥，长度至少为10个字符。(过短会报错)
     */
    private String tokenSignKey;

    /**
     * 请求头携带token指定的name。
     */
    private String tokenHeaderName;

    /**
     * 令牌Token在被刷新之后，服务器Http应答的header name，客户端或浏览器需要保存并替换原有的token，用于下次发送时携带
     */
    private String refreshedTokenHeaderKey;

    /**
     * token过期时间。
     */
    private Long tokenExpiration;

    /**
     * 授信ip列表，不填表示全部信任，多ip用逗号分隔。
     */
    private String credentialIpList;

    /**
     * Session会话和用户权限在Redis中的过期时间(秒)。
     * 缺省值是 one day
     */
    private int sessionExpiredSeconds = 86400;

    /**
     * 基于全等的url白名单地址集合，过滤效率高于whitelistUrlPattern。
     */
    private Set<String> whiteListUrl;

    /**
     * 基于Ant Pattern模式判定规则的白名单地址集合。如：/aa/**。
     */
    private Set<String> whitelistUrlPattern;
}
