package com.lt.gateway;

import com.lt.gateway.filter.AuthenticationPreFilter;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.Bean;

import java.util.Map;

/**
 * GatewayApplication
 *
 * @Author ToneyMa
 * @Date 2023-09-28
 **/
@EnableDiscoveryClient
@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class})
public class GatewayApplication {
    public static void main(String[] args) {
        SpringApplication.run(GatewayApplication.class, args);
    }

    @Bean
    public AuthenticationPreFilter authenticationPreFilter(){
        return new AuthenticationPreFilter();
    }
}
